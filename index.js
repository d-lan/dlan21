const express = require("express");
const app = express();

const port = 2834;

app.use(express.static("public"));

app.listen(port, () => {
  console.log("d-lan listening on https://localhost:" + port);
});
