# D-LAN's website 2020-2021

## Folder structure

- `./assets`: assets like images and fonts
- `./placeholder`: the temporary static website used before the real website was finished
- `./public`: the generated static files, served by the web server
- `./scripts`: compiled utility scripts.
- `./src`: source files, if you want to change something, you probably want to do it here
- `./src/html`: old and unused
- `./src/js`: unused
- `./src/scripts`: the typescript files which are compiled to the ./scripts folder
- `./src/scripts/html-generator`: a custom library for generating html from typescript code
- `./src/scripts/ts_html`: the source files for all the pages
- `./src/scss`: the scss files which are compiled to the ./public folder

## Useful files

- `./src/scripts/build.ts`: the source file for the build script
- `./src/scripts/webserver.ts`: the source file for the webserver
- `./package.json`: defines the dependencies and the available commands

## Command explanations

All commands are prefixed with `npm run`

- `build`: generate all the static content
- `start`: starts the server (used locally)
- `restart`: restarts the server (used to update the server)
- `clean`: delete all the static content

## Useful information

In the source file for the web server you will find a lot of disabled code.
Due to the changed circumstances, the needs also changed, and this site is now fully static instead.

The source files of the pages are located in `./src/scripts/ts_html`,
where they are written as TypeScript files.
These are then compiled to JavaScript,
after which the build command generates the html from the object that the files export.
I did it this way because I liked TypeScript at the time,
and it's much more comfortable to write in compared to html.
Also I had good fun implementing it :)

This project uses prettier and eslint as development tools,
to ensure the code works and is formatted correctly.
I recommend using plugins in your text editor which integrate which these tools automatically.

All the JavaScript files are compiled from TypeScript.

All the style sheets are compiled from scss to css.
You can read more about scss in the link provided below.

## Useful links

- [nodejs](https://nodejs.org)
- [npmjs](https://npmjs.com)
- [typescript](https://typescriptlang.org)
- [express](https://expressjs.com)
- [stripe](https://stripe.com)
- [sass](https://sass-lang.com)
- [eslint](https://eslint.org)
- [prettier](https://prettier.io)
