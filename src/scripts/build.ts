import fs from "fs";
import path from "path";
import prettier from "prettier";
import sass from "sass";

import html_pages from "./ts_html/index";

const buildfile_mtime = fs
  .statSync(path.join("scripts/build.js"))
  .mtime.getTime();

const src = path.join("src");
const dest = path.join("public");

const html_src = path.join(src, "scripts", "ts_html");

const scss_src = path.join(src, "scss");
const scss_main_file = path.join(scss_src, "main.scss");

const js_src = path.join(src, "js");

function make(
  src_file: string,
  dest_file: string,
  build: (data: string) => string,
  deps: number[] = []
): boolean {
  deps.push(fs.statSync(src_file).mtime.getTime());

  // Create destination folder, if it doesn't exist
  if (!fs.existsSync(path.parse(dest_file).dir))
    fs.mkdirSync(path.parse(dest_file).dir);

  // If source and boilerplate are younger than destination, continue
  if (
    fs.existsSync(dest_file) &&
    deps.sort((a, b) => b - a)[0] < fs.statSync(dest_file).mtime.getTime()
  ) {
    console.log(`Skipped file ${src_file}: ${dest_file} is newer`);
    return false;
  }

  // Write the built file to dest_file
  fs.writeFileSync(dest_file, build(src_file));
  console.log(`Created file ${dest_file} from ${src_file}`);
  return true;
}

/* ======== Create HTML ======== */

for (const html of html_pages) {
  make(
    path.join(html_src, html.src),
    html.dest == "index"
      ? path.join(dest, "index.html")
      : path.join(dest, html.dest, "index.html"),
    () =>
      prettier.format(html.data, {
        parser: "html",
      }),
    [buildfile_mtime]
  );
}

/* ======== Create CSS ======== */

// Get mtime for main
const scss_main_mtime = fs.statSync(scss_main_file).mtime.getTime();

for (const scss of fs.readdirSync(scss_src)) {
  // Skip main.scss
  if (scss == "main.scss") continue;

  // Create destination path
  const src_file = path.join(scss_src, scss);
  const dest_file =
    scss == "index.scss"
      ? path.join(dest, "style.css")
      : path.join(dest, path.parse(scss).name, "style.css");

  make(
    src_file,
    dest_file,
    (src_file) =>
      sass
        .renderSync({
          data: fs.readFileSync(src_file).toString(),
          includePaths: ["src/scss/"],
        })
        .css.toString(),
    [buildfile_mtime, scss_main_mtime]
  );
}

/* ======== Create JS ======== */

for (const js of fs.readdirSync(js_src)) {
  const src_file = path.join(js_src, js);
  const dest_file =
    js == "script.js"
      ? path.join(dest, "script.js")
      : path.join(dest, path.parse(js).name, "script.js");

  make(
    src_file,
    dest_file,
    (src_file) => fs.readFileSync(src_file).toString(),
    [buildfile_mtime]
  );
}
