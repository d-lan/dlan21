import fs from "fs";
import path from "path";

const filepath = path.join("data.json");

interface Database {
  places: string[];
  bookings: {
    [index: string]: {
      place: string;
      adress: string;
      mail: string;
    };
  }[];
}

fs.readFile(filepath, (err, data: Buffer) => {
  if (err) throw err;
  const db: Database = JSON.parse(data.toString());

  db.places = [];

  for (const room of ["T1", "T2", "T3", "T4"])
    for (let i = 0; i < 100; i++)
      db.places.push(`${room}:${i.toString().padStart(2, "0")}`);

  fs.writeFile(filepath, JSON.stringify(db), () =>
    console.log("Places generated!")
  );
});
