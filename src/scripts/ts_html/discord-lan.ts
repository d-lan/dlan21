import { makeHTML, e } from "../html-generator/element";

const tournaments: { name: string; players: string }[] = [
  { name: "CS:GO", players: "5v5" },
  { name: "Starcraft 2", players: "1v1" },
  { name: "Minecraft: Hunger Games", players: "FFA" },
  { name: "League of Legends", players: "5v5" },
  { name: "Rocket League", players: "3v3" },
];

const tournament_list = e.listGenerator(
  "ul",
  tournaments,
  (g: { name: string; players: string }) =>
    e.li(e.span(e.span(g.name), e.span(g.players)).class("between"))
);

const links: { name: string; link: string }[] = [
  {
    name: "AktUs discord-server",
    link: "https://discord.gg/NRyX3C7x9k",
  },
  {
    //   name: "Laganmälan",
    name: "Turneringsregler",
    link: "/laganmälan",
  },
  {
    name: "Turneringsstatus",
    link: "/turneringsstatus",
  },
  {
    name: "Livestream (Twitch)",
    link: "https://www.twitch.tv/dlan_liu",
  },
];

const link_list = e
  .listGenerator("div", links, (link: { name: string; link: string }) =>
    e.a(link.link, link.name)
  )
  .class("links");

export default {
  src: "discord-lan.ts",
  dest: "discord-lan",
  data: makeHTML(
    { title: "Discord-LAN" },
    e.body(
      e
        .div(
          e
            .div(
              e.h(1, "Välkommen till D(iscord)-LAN!"),
              e.p(
                e.i("Ah finally!"),
                e.br(),
                "Det är med stor glädje D-LAN, i samarbete med D-AktU och Y-AktU, presenterar ett sprillans nytt event som kommer ersätta det fysiska D-LAN för i år:"
              ),
              e
                .img("/assets/discord-lan_logo.png", "D(iscord)-LAN")
                .id("discord-lan")
            )
            .class("card"),
          e
            .div(
              e.h(2, "Vad är D(iscord)-LAN?"),
              e.p(
                "I helhet är det en spelkväll fylld med turneringar som du och dina vänner kan delta i,",
                "men även ett bra tillfälle för dig och dina kompisar att komma och ha lite skönlir.",
                "Turneringarna kommer att streamas live på Twitch (länk finns nedan),",
                "och turneringarna kommer vi ha två kommentatorer som livekommenterar matcherna.",
                "Mellan varje turnering bjuder vi även på pausunderhållning."
              ),
              e.h(2, "Var sker eventet?"),
              e.p(
                "Eventet kommer att hållas på AktU:s Discordserver.",
                "Du kan gå med i den genom att klicka på länken nedan."
              ),
              e.h(2, "När sker eventet?"),
              e.p(
                "D(iscord)-LANet kommer att börja 16:15 den 27 Mars.",
                "Var där eller var rektangulär!"
              )
            )
            .class("card"),
          e
            .div(
              e.h(1, "Turneringar"),
              e.p(
                "På D(iscord)-LANet kommer fem turneringar att erbjudas. Dessa är:"
              ),
              tournament_list,
              e.p(
                "För att anmäla er till turneringen, klicka på länken nedan och fyll i erat lag i formuläret.",
                e.b(
                  "Observera att platserna är begränsade, och först till kvarn gäller!"
                )
              )
            )
            .class("card"),
          e.div(e.h(1, "Länkar"), link_list).class("card")
        )
        .class("page")
    )
  ),
};
