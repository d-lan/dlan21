import { makeHTML, e } from "../html-generator/element";

interface tournament {
  name: string;
  id: string;
  link: string;
}

const tournaments: tournament[] = [
  {
    name: "CS:GO",
    id: "cs-go",
    link: "https://challonge.com/dlan2021csgo",
  },
  {
    name: "StarCraft 2",
    id: "starcraft-2",
    link: "https://challonge.com/dlan2021sc2",
  },
  {
    name: "League of Legends",
    id: "league-of-legends",
    link: "https://challonge.com/dlan2021lol",
  },
  {
    name: "Rocket League",
    id: "rocket-league",
    link: "https://challonge.com/dlan2021rl",
  },
];

const tournament_list = tournaments.map((t: tournament) =>
  e
    .div(
      e.h(1, t.name),
      e
        .iframe(t.link + "/module?scale_to_fit=1&show_final_results=1")
        .class("challonge")
        .addAttributes({ allowTransparency: "true", frameBorder: "0" })
    )
    .class("card")
    .id(t.id)
);

export default {
  src: "turneringsstatus.ts",
  dest: "turneringsstatus",
  data: makeHTML(
    { title: "Turneringsstatus" },
    e.body(e.div(...tournament_list).class("page"))
  ),
};
