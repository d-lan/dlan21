import index from "./main";
// import discord_lan from "./discord-lan";
// import laganmälan from "./laganmälan";
// import turneringsstatus from "./turneringsstatus";

const html_pages: { src: string; dest: string; data: string }[] = [
  index,
  // discord_lan,
  // laganmälan,
  // turneringsstatus,
];

export default html_pages;
