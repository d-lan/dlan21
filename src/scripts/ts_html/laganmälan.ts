import { makeHTML, e, WebElement } from "../html-generator/element";

const cs_go_rules = e.list(
  "ol",
  e.li(
    e.h(2, "Tournament"),
    e.ul(
      "Format: Single Elimination",
      "All games are best of one",
      "Teams are 5v5"
    )
  ),
  e.li(
    e.h(2, "General rules"),
    e.ul(
      "The captain is needed to give complete and full information on all the participants in his/her team.",
      "Player Agents are not allowed",
      "All captains need to be reachable during the tournament. \
        IMPORTANT: If the other team has not accepted or joined your game within 10 minutes, \
        you are to report this to the referee and he will see if the team is absent \
        (If the players are absent they will be notified by some means. \
        If the captain fails to respond within 5 minutes the team automatically loses the match).",
      "If the Server would by any chance freeze mid-game or crash the referee will decide whether the game is to be restarted or have any other outcome.",
      "If a player should disconnect while playing, \
        stop playing and the referee will pause the round as fast as possible. \
        If the player does not connect within 10 minutes and the team does not have a substitute, \
        the team needs to keep playing with only four players. \
        This does not apply if the problem is caused by D-LAN’s servers.",
      "Coin toss will decide who starts on which side. \
        The captain whose team is in the upper bracket slot sends either heads or tails to the judge who does the flip. \
        The coin toss winner also starts banning maps, you take turns banning maps until there is only one left."
    )
  ),
  e.li(
    e.h(2, "Maps"),
    e.list(
      "ul",
      e.li(
        "Maps that will be used are:",
        e.ul(
          "de_dust2",
          "de_nuke",
          "de_train",
          "de_mirage",
          "de_inferno",
          "de_overpass",
          "de_vertigo"
        )
      )
    )
  ),
  e.li(
    e.h(2, "Setup"),
    e.p(
      "Team captains will be provided with a server IP address that your team should join. \
        Both teams should send a message via Discord that they’re ready to the match referee who will then stop the warmup and start the game. \
        The winner reports back to the referee that they won."
    )
  ),
  e.li(
    e.h(2, "Cheats"),
    e.p(
      "Generally scripts are not allowed by any means except Buy, toggle och demo scripts. \
        Here are some examples of illegal scripts:"
    ),
    e.ul(
      "Stop shoot script (Use or AWP scripts)",
      "Center view scripts",
      "Turn scripts (180 degree or similar)",
      "No recoil scripts",
      "Burst fire scripts",
      "Rate changers (Lag scripts)",
      "FPS scripts",
      "Anti-flash scripts or binding(snd_* bindings)",
      "Bunny hop scripts",
      "Stop sound scripts"
    ),
    e.p(
      "IMPORTANT: Remember this is a game we all love and cherish and it would not help anyone to make wrong accusations, \
        as cheating or other similar things. We play this to have a good time together, so make it count! \
        If there still is a strong feeling that someone is cheating make sure to report it to the Head Referee (Överdommare) or to the referee at hand and he will report it further. \
        Thanks in advance!"
    )
  )
);

const starcraft_2_rules = e.list(
  "ol",
  e.li(
    e.h(2, "Tournament"),
    e.ul(
      "Format: Single Elimination",
      "All games are best of three",
      "Teams are 1v1"
    )
  ),
  e.li(
    e.h(2, "General Rules"),
    e.ul(
      "All players need to be reachable during the tournament. \
        IMPORTANT: If the other player has not accepted or joined your game within 10 minutes, \
        you are to report this to the referee and he will see if the player is absent (If the players are absent they will be notified by some means. \
        If the player fails to respond within 5 minutes the player automatically loses the match).",
      "If the Server would by any chance freeze mid-game or crash the referee will decide whether the game is to be restarted or have any other outcome.",
      "Coin toss will decide who picks first. The player in the upper bracket slot sends either heads or tails to the judge who does the flip."
    )
  ),
  e.li(
    e.h(2, "Maps"),
    e.p("Maps that will be used are:"),
    e.ul(
      "Deathaura LE",
      "Jagannatha LE",
      "Lightshade LE",
      "Oxide LE",
      "Pillars of Gold LE",
      "Romanticide LE",
      "Submarine LE"
    ),
    e.p(
      "In best of 3 games, the coin toss winner decides which player starts the banning and picking process which proceeds like this:"
    ),
    e.ul(
      "Winner bans one map",
      "Loser bans one map",
      "Winner picks one map (Which will be in the first game)",
      "Loser picks one map (Which will be in the second game)",
      "Winner bans one map",
      "Loser bans one map",
      "The map that is left will then be the final map"
    )
  ),
  e.li(
    e.h(2, "Setup"),
    e.p(
      "Contact your opponent via Discord (you will find their Discord name in the tournament bracket) so you can exchange Blizzard IDs and set up the game, \
        if you have any questions or concerns contact the referee on discord. \
        The winner reports back to the referee that they won."
    ),
    e.p(
      "IMPORTANT: Remember this is a game we all love and cherish and it would not help anyone to make wrong accusations, \
        as cheating or other similar things. We play this to have a good time together, so make it count! \
        If there still is a strong feeling that someone is cheating make sure to report it to the Head Referee (Överdommare) or to the referee at hand and he will report it further. \
        Thanks in advance!"
    )
  )
);

const mc_hunger_rules = e.list(
  "ol",
  e.li(
    e.h(2, "Tournament"),
    e.ul("The game will be best of one", "The game is free for all")
  ),
  e.li(
    e.h(2, "General Rules"),
    e.ul(
      "All players need to be reachable during the tournament. \
            IMPORTANT: If you are not present or have contacted the referee within 5 minutes of the set start time you will be disqualified.",
      "If the Server would by any chance freeze mid-game or crash the referee will decide whether the game is to be restarted or have any other outcome.",
      "Prizes will be awarded to first and second place.",
      "If the winners have not been decided within 25 minutes an enchanted golden helmet will be placed in the middle of the map, \
        this action will be announced with a 5 minute warning. \
        The player in possession of the helmet must have it equipped at all times. \
        If a player holds the helmet for 3 minutes they are declared the winner.",
      "If there are more than 2 players alive when the game ends via “helmet possession” then second place is awarded to the person with the most amount of kills.",
      "Teaming or making alliances is not allowed, its everyone for themself"
    )
  ),
  e.li(
    e.h(2, "Setup"),
    e.p(
      "The server link will be posted in discord, make sure to have the most recent version of Minecraft installed. \
        When you have joined the server refrain from attacking other players or taking damage since this will slow down the start of the game."
    )
  ),
  e.li(
    e.h(2, "Cheats"),
    e.p("Only the vanilla client and textures are allowed"),
    e.p(
      "IMPORTANT: Remember this is a game we all love and cherish and it would not help anyone to make wrong accusations, \
        as cheating or other similar things. We play this to have a good time together, so make it count! \
        If there still is a strong feeling that someone is cheating make sure to report it to the Head Referee (Överdommare) or to the referee at hand and he will report it further. \
        Thanks in advance!"
    )
  )
);

const lol_rules = e.list(
  "ol",
  e.li(
    e.h(2, "Tournament"),
    e.ul(
      "Format: Single elimination",
      "All games are best of one",
      "Teams are 5v5",
      "Map is Summoner's Rift"
    )
  ),
  e.li(
    e.h(2, "General Rules"),
    e.ul(
      "The captain is needed to give complete and full information on all the participants in his/her team.",
      "All players must have an EU-West valid account.",
      "All captains need to be reachable during the tournament. \
        IMPORTANT: If the other team has not accepted or joined your game within 10 minutes, \
        you are to report this to the referee and he will see if the team is absent \
        (If the players are absent they will be notified by some means. \
        If the captain fails to respond within 5 minutes the team automatically loses the match).",
      "If the Server would by any chance freeze mid-game or crash the referee will decide whether the game is to be restarted or have any other outcome.",
      "If a player should disconnect while playing the referee will pause the game as fast as possible. \
        If the player does not connect within 10 minutes and the team does not have a substitute, \
        the team needs to keep playing with only four players.",
      "Coin toss will decide who starts on which side (red or blue). \
        The captain whose team is in the upper bracket slot sends either heads or tails to the judge who does the flip."
    )
  ),
  e.li(
    e.h(2, "Setup"),
    e.p(
      "Contact your opponent’s team captain via Discord (you will find their Discord name in the tournament bracket) \
        so you can exchange League of Legends IDs and set up the game, if you have any questions or concerns contact the referee on discord. \
        The winner reports back to the referee that they won."
    ),
    e.p(
      "IMPORTANT: Remember this is a game we all love and cherish and it would not help anyone to make wrong accusations, \
        as cheating or other similar things. We play this to have a good time together, so make it count! \
        If there still is a strong feeling that someone is cheating make sure to report it to the Head Referee (Överdommare) or to the referee at hand and he will report it further. \
        Thanks in advance!"
    )
  )
);

const rocket_rules = e.list(
  "ol",
  e.li(
    e.h(2, "Tournament"),
    e.ul(
      "Format: Single elimination",
      "All games are best of three",
      "Teams are 3v3"
    )
  ),
  e.li(
    e.h(2, "General Rules"),
    e.ul(
      "The captain is needed to give complete and full information on all the participants in his/her team.",
      "All games are played in the Standard format",
      "All captains need to be reachable during the tournament. \
        IMPORTANT: If the other team has not accepted or joined your game within 10 minutes, \
        you are to report this to the referee and he will see if the team is absent \
        (If the players are absent they will be notified by some means. \
        If the captain fails to respond within 5 minutes the team automatically loses the match).",
      "If the Server would by any chance freeze mid-game or crash the referee will decide whether the game is to be restarted or have any other outcome.",
      "If a player should disconnect while playing the referee will pause the game as fast as possible. \
        If the player does not connect within 10 minutes and the team does not have a substitute, \
        the team needs to keep playing with only four players.",
      "Coin toss will decide who starts on which side. \
        The captain whose team is in the upper bracket slot sends either heads or tails to the judge who does the flip."
    )
  ),
  e.li(
    e.h(2, "Maps"),
    e.p("Maps that will be used are:"),
    e.ul(
      "DFH Stadium",
      "Utopia Coliseum",
      "Wasteland",
      "Champions Field",
      "AquaDome",
      "Famrstead",
      "Salty Shores"
    ),
    e.p(
      "In best of 3 games, the coin toss winner decides which player starts the banning and picking process which proceeds like this:"
    ),
    e.ul(
      "Winning team bans one map",
      "Losing team bans one map",
      "Winning team picks one map (Which will be in the first game)",
      "Losing team picks one map (Which will be in the second game)",
      "Winning team bans one map",
      "Losing team bans one map",
      "The map that is left will then be the final map"
    )
  ),
  e.li(
    e.h(2, "Setup"),
    e.p(
      "Contact your opponent’s team captain via Discord (you will find their Discord name in the tournament bracket) \
        so you can exchange Steam IDs and set up the game, if you have any questions or concerns contact the referee on discord. \
        The winner reports back to the referee that they won."
    ),
    e.p(
      "IMPORTANT: Remember this is a game we all love and cherish and it would not help anyone to make wrong accusations, \
        as cheating or other similar things. We play this to have a good time together, so make it count! \
        If there still is a strong feeling that someone is cheating make sure to report it to the Head Referee (Överdommare) or to the referee at hand and he will report it further. \
        Thanks in advance!"
    )
  )
);

interface tournament {
  name: string;
  id: string;
  players: string;
  rules: WebElement;
  link: WebElement;
}

const tournaments: tournament[] = [
  {
    name: "CS:GO",
    id: "cs_go",
    players: "5v5",
    rules: cs_go_rules,
    // link: e.a("https://forms.gle/QQXmhEWVYGMCiTbFA", "Anmäl lag").class("link"),
    link: e.p("Anmälan stängd"),
  },
  {
    name: "Starcraft 2",
    id: "starcraft_2",
    players: "1v1",
    rules: starcraft_2_rules,
    // link: e.a("https://forms.gle/3txcnMBtBpgiQnnm9", "Anmäl lag").class("link"),
    link: e.p("Anmälan stängd"),
  },
  {
    name: "Minecraft: Hunger Games",
    id: "mc_hunger_games",
    players: "FFA",
    rules: mc_hunger_rules,
    link: e.h(4, "Laganmälan sker på plats"),
  },
  {
    name: "League of Legends",
    id: "lol",
    players: "5v5",
    rules: lol_rules,
    // link: e.a("https://forms.gle/6DkNYTmx4NpWKWNw6", "Anmäl lag").class("link"),
    link: e.p("Anmälan stängd"),
  },
  {
    name: "Rocket League",
    id: "rocket_league",
    players: "3v3",
    rules: rocket_rules,
    // link: e.a("https://forms.gle/msn6pJHvG9644aAB7", "Anmäl lag").class("link"),
    link: e.p("Anmälan stängd"),
  },
];

const quick_menu_list = e
  .listGenerator("div", tournaments, (t: tournament) => e.a("#" + t.id, t.name))
  .class("links");

const tournament_list = tournaments.map((t: tournament) =>
  e
    .div(e.h(1, `${t.name} - ${t.players}`), t.rules, t.link)
    .class("card")
    .id(t.id)
);

export default {
  src: "laganmälan.ts",
  dest: "laganmälan",
  data: makeHTML(
    { title: "Laganmälan" },
    e.body(
      e
        .div(
          e.div(e.h(1, "Genvägar"), quick_menu_list).class("card"),
          ...tournament_list
        )
        .class("page")
    )
  ),
};
