import { makeHTML, e } from "../html-generator/element";

interface social_link {
  name: string;
  link: string;
}

const social_links: social_link[] = [
  {
    name: "twitch",
    link: "https://www.twitch.tv/dlan_liu",
  },
  {
    name: "facebook",
    link: "https://www.facebook.com/DLANLiTH/",
  },
  // {
  //   name: "discord",
  //   link: "", // TODO: fill me
  // },
];

interface person {
  name: string;
  post: string;
}

const staff: person[] = [
  {
    name: "Arvid Westerlund",
    post: "Projektledare",
  },
  {
    name: "Adrian Byström",
    post: "Personalwerk",
  },
  {
    name: "Anthony Boer",
    post: "Spons & Event",
  },
  {
    name: "Gabriel Hult",
    post: "Tryck & PR",
  },
  {
    name: "Simon Gutgesell",
    post: "Webb & Stream",
  },
  {
    name: "Rasmus Wallin",
    post: "Nät",
  },
  {
    name: "Jakob Österberg",
    post: "Elwerk",
  },
  {
    name: "Viktor Norgren",
    post: "Turnering & Biljett",
  },
];

const social_links_list = e
  .listGenerator("div", social_links, (s: social_link) =>
    e.a(s.link, e.img(`/assets/${s.name}.svg`, s.name)).id(s.name)
  )
  .class("social-links");

const staff_list = e
  .listGenerator("div", staff, (p: person) => {
    const img = p.name.split(" ")[0].toLowerCase();
    return e.div(
      e.img(`/assets/faces/${img}.jpg`, img),
      e.h(5, p.name),
      e.h(6, p.post)
    );
  })
  .class("staff-list");

export default {
  src: "main.ts",
  dest: "index",
  data: makeHTML(
    { title: "D-LAN 2021" },
    e.body(
      e
        .div(
          e.img("/assets/logo.svg", "Logga").class("logo"),
          // e.h(4, "Tagga D(iscord)-LAN!!!").id("date"),
          e.h(4, "Tagga D-LAN 2022!!!").id("date"),
          // e.div(e.a("/discord-lan", "Vad är D(iscord)-LAN?")).class("menu"),
          social_links_list,
          e
            .a("#news", e.img("/assets/icons/down-arrow.svg", "Go down"))
            .id("go-down")
        )
        .id("front-page"),
      // e
      //   .div(
      //     e
      //       .div(
      //         e.h(1, "D-LAN på distans"),
      //         e.p(
      //           "På grund av COVID-19 kan vi tyvärr inte ha ett D-LAN i år. \
      //           Istället kommer det bli ett D(iscord)-LAN i Akt-Us discord-server. \
      //           Klicka på knappen ovan för att läsa mer om du är intresserad."
      //         )
      //       )
      //       .class("card")
      //   )
      //   .id("news")
      //   .class("page"),
      e
        // .div(e.h(1, "Vilka är vi som jobbar med D-LAN?"), staff_list)
        .div(e.h(1, "Vilka var vi som jobbade med D-LAN 2021?"), staff_list)
        .id("about")
        .class("page")
    )
  ),
};
