import express from "express";

const app = express();

// Port number for the webserver.
const port = 2834; // Must match the port number in the nginx config

app.use("/", express.static("placeholder"));

app.listen(port, () =>
  console.log("d-lan listening on http://localhost:" + port)
);
