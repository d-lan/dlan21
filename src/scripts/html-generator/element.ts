type WebElementAttributes = { [index: string]: string };
type WebElementContent = (string | WebElement)[];

export class WebElement {
  private tag_name: string;
  private attributes: WebElementAttributes;
  private content: WebElementContent;

  constructor(
    tag: string,
    attributes: WebElementAttributes,
    content: WebElementContent
  ) {
    this.tag_name = tag;
    this.attributes = attributes;
    this.content = content;
  }

  public toString(): string {
    return (
      "<" +
      this.tag_name +
      (Object.keys(this.attributes).length
        ? " " +
          Object.keys(this.attributes)
            .map((key) =>
              this.attributes[key] ? `${key}="${this.attributes[key]}"` : key
            )
            .join(" ")
        : "") +
      (this.content.length
        ? ">" +
          this.content
            .map((element) =>
              typeof element == "string" ? element : element.toString()
            )
            .join("") +
          "</" +
          this.tag_name +
          ">"
        : " />")
    );
  }

  private assertTagHasAttribute(
    allowed: string[],
    attribute_name: string
  ): void {
    if (!allowed.includes(this.tag_name))
      throw `Element ${this.tag_name} does not use the ${attribute_name} attribute.`;
  }

  // Add attributes
  public addAttributes(attributes: WebElementAttributes): WebElement {
    this.attributes = Object.assign(this.attributes, attributes);
    return this;
  }

  // Add content
  public add(content: WebElementContent): WebElement {
    this.content.concat(content);
    return this;
  }

  // Shouldn't be used. Use this.css instead
  public class(c: string | string[]): WebElement {
    return this.addAttributes({
      class: typeof c == "string" ? c : c.join(" "),
    });
  }

  public id(id: string): WebElement {
    return this.addAttributes({ id: id });
  }

  public src(src: string): WebElement {
    this.assertTagHasAttribute(
      [
        "audio",
        "embed",
        "iframe",
        "img",
        "input",
        "script",
        "source",
        "track",
        "video",
      ],
      "src"
    );
    return this.addAttributes({ src: src });
  }

  public href(href: string): WebElement {
    this.assertTagHasAttribute(["a", "area", "base", "link"], "href");
    return this.addAttributes({ href: href });
  }

  public rel(rel: string): WebElement {
    this.assertTagHasAttribute(["a", "area", "form", "link"], "rel");
    return this.addAttributes({ rel: rel });
  }

  public lang(lang: string): WebElement {
    return this.addAttributes({ lang: lang });
  }
}

function elem(name: string, ...content: WebElementContent): WebElement {
  return new WebElement(name, {}, content);
}

export const e: {
  [index: string]: (...args: any) => WebElement;
} = {
  docstring: () => elem("!DOCTYPE html"),
  html: (...content: WebElementContent) => elem("html", ...content),
  head: (...content: WebElementContent) => elem("head", ...content),
  meta: (name: string, content: string) =>
    elem("meta").addAttributes({
      name: name,
      content: content,
    }),
  link: (rel: string, href: string) => elem("link").rel(rel).href(href),
  stylesheet: (href: string) => elem("link").rel("stylesheet").href(href),
  title: (content: string) => elem("title", content),
  body: (...content: WebElementContent) => elem("body", ...content),
  h: (n: number, content: string) => elem(`h${n}`, content),
  p: (...content: string[]) => elem("p", content.join(" ")),
  a: (href: string, content: string) => elem("a", content).href(href),
  i: (content: string) => elem("i", content),
  b: (content: string) => elem("b", content),
  img: (src: string, alt: string) =>
    elem("img").src(src).addAttributes({ alt: alt }),
  listGenerator: (
    type: string,
    data: any[],
    generator: (...args: any) => WebElement
  ) => elem(type, ...data.map(generator)),
  list: (type: string, ...content: WebElementContent) => elem(type, ...content),
  ol: (...content: WebElementContent) =>
    elem("ol", ...content.map((c) => elem("li", c))),
  ul: (...content: WebElementContent) =>
    elem("ul", ...content.map((c) => elem("li", c))),
  li: (...content: WebElementContent) => elem("li", ...content),
  div: (...content: WebElementContent) => elem("div", ...content),
  span: (...content: string[]) => elem("span", ...content),
  br: () => elem("br"),
  iframe: (src: string) => elem("iframe", "").src(src),
};

export function makeHTML(head: { title: string }, body: WebElement): string {
  return (
    e.docstring().toString() +
    e
      .html(
        e.head(
          e.title(head.title),
          e.meta("viewport", "width=device-width, initial-scale=1"),
          e.meta("theme-color", "#001d51"),
          e.link("preconect", "https://fonts.gstatic.com"),
          e.stylesheet(
            "https://fonts.googleapis.com/css2?family=Exo:wght@900&family=Lato:wght@700&display=swap"
          ),
          e.stylesheet("style.css")
        ),
        body
      )
      .lang("sv")
      .toString()
  );
}
