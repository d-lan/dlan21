//TODO: Move database to memory, writing it to disk on save

// import fs from "fs";
import express from "express";
// import bodyParser from "body-parser";
// import Stripe from "stripe";

// const stripe = new Stripe("sk_test_PBrYh2JPAf8V8fKxf8O5ihLo", {
//   apiVersion: "2020-08-27",
//   typescript: true,
// }); // Change this key to the correct key

/* ======== Constants ======== */

const app = express();

// Port number for the webserver.
const port = 2834; // Must match the port number in the nginx config

// const database = "data.json";

// function calculateOrderAmount(order: { ticket: boolean; pickup: boolean }) {
//   let price = 0;
//   if (order.ticket) price += 100; // TODO: Changeme
//   if (order.pickup) price += 100; // TODO: Changeme
//   // Price is used in ören, rather than kronor
//   return price * 100;
// }

/* ======== Webserver configuration ======== */

// Expose directories
app.use("/", express.static("public"));
app.use("/assets", express.static("assets"));

// app.use(express.json());
// // Parse incoming form submissions, as described by:
// // https://medium.com/@paupavon/handling-form-submissions-in-nodejs-876bc980dc0a
// app.use(bodyParser.urlencoded({ extended: true }));

// // Respond to request of available places
// app.get("/available-places", (_req, res) => {
//   fs.readFile(database, (err, data) => {
//     if (err) throw err;
//     const places = JSON.parse(data.toString());
//     res.send(JSON.stringify(places));
//   });
// });

// // Respond to booking submissions
// app.post("/submit", (req, res) => {
//   // Add form submission to db
//   fs.readFile(database, (err, data) => {
//     if (err) throw err;

//     // Read database
//     const db = JSON.parse(data.toString());

//     // Check form submission
//     // Make sure this is unbreakable
//     const error =
//       !(
//         req.body.id.match(/^[a-z]{5}\d{3}$/g) &&
//         req.body.place.match(/^T[1-4]:\d{2}/g)
//       ) || db.bookings[req.body.id];

//     // Something's wrong, go back
//     if (error) res.send("dont haxx me pls");
//     // All good
//     else {
//       // Respond to client, as fast as possible
//       res.send("public/success/index.html");

//       // Insert booking into database
//       db.bookings[req.body.id] = {
//         place: req.body.place,
//         address: req.body.address,
//         mail: req.body.mail,
//       };

//       // Write database
//       fs.writeFile(database, JSON.stringify(db), (err) => {
//         if (err) throw err;
//       });
//     }
//   });
//   console.log(req.body);
// });

// // Respond to payment
// app.post("/create-payment-intent", async (req, res) => {
//   const order = req.body;

//   const paymentIntent = await stripe.paymentIntents.create({
//     amount: calculateOrderAmount(order),
//     currency: "sek",
//   });

//   res.send({
//     clientSecret: paymentIntent.client_secret,
//   });
// });

// Start server
app.listen(port, "0.0.0.0", () =>
  console.log("d-lan listening on http://localhost:" + port)
);
