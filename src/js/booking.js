// This file has mostly been copied from `client.js' on the following website:
// https://stripe.com/docs/payments/integration-builder

const stripe = Stripe("pk_test_wWE09MKZmSevpogsRsNE07mb"); // Change me to the real key

const checkout_button = document.getElementById("submit");

// The default selected products
const purchase = {
    ticket: true,
};

checkout_button.disabled = true;

fetch("/create-payment-intent", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify(purchase),
})
    .then((result) => result.json())
    .then((data) => {
        const elements = stripe.elements();

        // Style goes here:
        const style = { hidePostalCode: true };

        const card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.on("change", function (event) {
            // Disable the Pay button if there are no card details in the Element
            checkout_button.disabled = event.empty;
            document.querySelector("#card-error").textContent = event.error
                ? event.error.message
                : "";
        });

        var form = document.getElementById("payment-form");

        form.addEventListener("submit", function (event) {
            event.preventDefault();
            // Complete payment when the submit button is clicked
            payWithCard(stripe, card, data.clientSecret);
        });
    });

// Calls stripe.confirmCardPayment
// If the card requires authentication Stripe shows a pop-up modal to
// prompt the user to enter authentication details without leaving your page.
function payWithCard(stripe, card, clientSecret) {
    loading(true);
    stripe
        .confirmCardPayment(clientSecret, {
            payment_method: {
                card: card,
            },
        })
        .then(function (result) {
            if (result.error) {
                // Show error to your customer
                showError(result.error.message);
            } else {
                // The payment succeeded!
                orderComplete(result.paymentIntent.id);
            }
        });
}

/* ------- UI helpers ------- */
// Shows a success message when the payment is complete
function orderComplete(paymentIntentId) {
    loading(false);
    document
        .querySelector(".result-message a")
        .setAttribute(
            "href",
            "https://dashboard.stripe.com/test/payments/" + paymentIntentId
        );
    document.querySelector(".result-message").classList.remove("hidden");
    checkout_button.disabled = true;
}

// Show the customer the error from Stripe if their card fails to charge
function showError(errorMsgText) {
    loading(false);
    var errorMsg = document.querySelector("#card-error");
    errorMsg.textContent = errorMsgText;
    setTimeout(function () {
        errorMsg.textContent = "";
    }, 4000);
}

// Show a spinner on payment submission
function loading(isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        checkout_button.disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
    } else {
        checkout_button.disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
}
